@extends('layouts.app')
@section('title','Slider')
@push('css')
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
@endpush
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.partial.msg')
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title mt-0"> All Contact Message</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead class="">
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Sent At</th>
                                    <th>Action</th>
                                </thead>
                                 <tbody>
                                @foreach($contacts as $key=>$contact)
                                <tr>
                                    <td>{{ $key + 1}}</td>
                                    <td>{{ $contact->name}}</td>
                                    <td>{{ $contact->email}}</td>
                                    <td>{{ $contact->subject}}</td>
                                    <td>{{ $contact->message}}</td>
                                    <td>{{ $contact->updated_at}}</td>
                                    <td>
                                        <a href="{{ route('contact.show',$contact->id) }}" class="btn btn-info btn-sm"><i class="material-icons">details</i></a>
                                        
                                        <form id="delete-form-{{ $contact->id }}" action="{{ route('contact.destory',$contact->id) }}" style="display: none;" method="POST">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                        </form>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $contact->id }}').submit();
                                        }else {
                                            event.preventDefault();
                                        }"><i class="material-icons">delete</i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
$('#table').DataTable();
} );
</script>
@endpush