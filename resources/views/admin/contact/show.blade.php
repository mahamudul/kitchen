@extends('layouts.app')
@section('title','Slider')
@push('css')
@endpush
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.partial.msg')
                <div class="card card-plain">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title mt-0">{{ $contact->subject }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="card-contend">
                            <div class="row">
                                <div class="col-md-8">
                                    <strong>Name: {{ $contact->name }}</strong><br>
                                    <b>Email: {{ $contact->email }}</b><hr/>
                                    <strong>Message: </strong><br/>
                                    <p>{{ $contact->message }}</p><hr>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('contact.index')}}" class="btn btn-danger">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(document).ready(function() {
$('#table').DataTable();
} );
</script>
@endpush