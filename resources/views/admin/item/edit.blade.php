@extends('layouts.app')

@section('title','Item')

@push('css')

@endpush

@section('content')
   <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               @include('layouts.partial.msg')
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Category</h4>
                </div>
                <div class="card-body">
                <form method="POST" action="{{ route('item.update',$item->id) }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  @method('PUT')
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Category</label>
                          <select class="form-control" name="category">
                            @foreach($categories as $categorie)
                              <option {{ $categorie->id == $item->category->id ? 'selected' : '' }} value="{{$categorie->id}}">{{$categorie->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Item Name</label>
                          <input type="text" name="name" value="{{ $item->name }}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="form-group">
                            <label class="bmd-label-floating">Description</label>
                            <textarea name="description" class="form-control" rows="5">
                              {{ $item->description }}
                            </textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Price</label>
                          <input type="number" name="price" value="{{ $item->price }}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="">
                          <label class="bmd-label-floating">Image</label>
                          <input type="file" name="image">
                        </div>
                      </div>
                    </div>
                    <button type="submit" name="save" class="btn btn-primary pull-right">Save</button>
                    <a href="{{ route('item.index') }}" class="btn btn-danger">Back</a>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('scripts')

@endpush