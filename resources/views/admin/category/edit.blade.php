@extends('layouts.app')

@section('title','Category')

@push('css')

@endpush

@section('content')
   <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               @include('layouts.partial.msg')
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Category</h4>
                </div>
                <div class="card-body">
                 <form method="POST" action="{{ route('category.update',$category->id) }}" >
                  {{ csrf_field() }}
                  @method("PUT")
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          <input type="text" name="name" value="{{$category->name}}" class="form-control">
                        </div>
                      </div>
                    </div>
                    <button type="submit" name="save" class="btn btn-primary pull-right">Save</button>
                    <a href="{{ route('category.index') }}" class="btn btn-danger">Back</a>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('scripts')

@endpush