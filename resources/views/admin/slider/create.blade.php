@extends('layouts.app')

@section('title','Slider')

@push('css')

@endpush

@section('content')
   <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               @include('layouts.partial.msg')
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add New Slider</h4>
                </div>
                <div class="card-body">
                 <form method="POST" action="{{ route('slider.store') }}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          <input type="text" name="title" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Sub Title</label>
                          <input type="text" name="sub_title" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="">
                          <label class="bmd-label-floating">Image</label>
                          <input type="file" name="image">
                        </div>
                      </div>
                    </div>
                    <button type="submit" name="save" class="btn btn-primary pull-right">Save</button>
                    <button type="submit" name="back" class="btn btn-danger pull-right">Back</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@push('scripts')

@endpush