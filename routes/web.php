<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'HomeController@index')->name('welcome');
Route::post('/reservation','ReservationController@reserve')->name('reservation.reserve');

/*contct Route*/
Route::post('/contact','ContactController@sendMessage')->name('contact.send');
Auth::routes();

Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'admin'], function(){
	Route::get('dashboard','DashboardController@index')->name('admin.dashboard');
	/*slider Controller*/
	Route::resource('slider','SliderController');
	/*Category Controller*/
	Route::resource('category','CategoryController');
	/*ItemController*/
	Route::resource('item','ItemController');
	/*Reservation*/
	Route::get('reservation','ReservarionController@index')->name('reservation.index');
	Route::post('reservation/{id}','ReservarionController@status')->name('reservation.status');
	Route::delete('reservation/{id}','ReservarionController@destory')->name('reservation.destory');


	/*Admin Controller*/
	Route::get('contact','ContactController@index')->name('contact.index');
	Route::get('contact/{id}','ContactController@show')->name('contact.show');
	Route::delete('contact/{id}','ContactController@destory')->name('contact.destory');
});

